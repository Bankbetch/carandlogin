package com.example.car


import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.car.databinding.FragmentCarListBinding
import com.example.car.ui.main.Car

/**
 * A simple [Fragment] subclass.
 */
class carList : Fragment() {

    private lateinit var binding:FragmentCarListBinding
    private val booking: ArrayList<Car> = ArrayList()
    private var car: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_car_list, container, false)
        for(x in 0..2){
            booking.add(x, Car("","",""))
        }

        binding.apply {
            btnS1.setBackgroundResource(android.R.drawable.btn_default);
            btnS2.setBackgroundResource(android.R.drawable.btn_default);
            btnS3.setBackgroundResource(android.R.drawable.btn_default);
        }
        if (car == 0) {
            binding.btnS1.setText("ว่าง")
            booking.removeAt(0)
            booking.add(0,Car("","",""));
        } else if (car == 1) {
            binding.btnS2.setText("ว่าง")
            booking.removeAt(1)
            booking.add(1,Car("","",""));
        } else if (car == 2) {
            binding.btnS3.setText("ว่าง")
            booking.removeAt(2)
            booking.add(2,Car("","",""));
        }
        set()
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            view!!.findNavController()
        )
                || super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)
    }


    private fun set(){
        binding.apply {
            btnS1.setOnClickListener {
                car = 0;
                btnS1.setBackgroundColor(Color.GRAY)
                btnS2.setBackgroundResource(android.R.drawable.btn_default);
                btnS3.setBackgroundResource(android.R.drawable.btn_default);
                    showValue(car);
            }
            btnS2.setOnClickListener {
                btnS2.setBackgroundColor(Color.GRAY)
                btnS1.setBackgroundResource(android.R.drawable.btn_default);
                btnS3.setBackgroundResource(android.R.drawable.btn_default);
                car = 1;
                    showValue(car);
            }
            btnS3.setOnClickListener {
                btnS3.setBackgroundColor(Color.GRAY)
                btnS1.setBackgroundResource(android.R.drawable.btn_default);
                btnS2.setBackgroundResource(android.R.drawable.btn_default);
                car = 2;
                  showValue(car);

            }
            submit.setOnClickListener {
                setValue(it)
            }
        }
    }
    private fun showValue(slot: Int) {
        binding.number.setText(booking.get(car).number)
        binding.band.setText(booking.get(car).brand)
        binding.name.setText(booking.get(car).name)
    }

    private fun setValue(view:View) {
        val registrationnumber = binding.number.text.toString()
        val brand = binding.band.text.toString()
        val name = binding.name.text.toString()
        booking.add(car , Car(registrationnumber, brand, name))
        if (car == 0) {
            binding.btnS1.setText("เต็ม")
        } else if (car == 1) {
            binding.btnS2.setText("เต็ม")
        } else {
            binding.btnS3.setText("เต็ม")
        }

    }
}
