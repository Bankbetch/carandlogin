package com.example.car.ui.main

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.car.R
import kotlinx.android.synthetic.main.main_fragment.*
import com.example.car.databinding.MainFragmentBinding
class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: MainFragmentBinding = DataBindingUtil.inflate(
            inflater, R.layout.main_fragment, container, false)

        binding.btnLogin.setOnClickListener{view: View->
            binding.apply {
                var user = username.text.toString()
                var pass = password.text.toString()
                if(user == "admin" && pass == "admin"){
                    view.findNavController()
                        .navigate(R.id.carList)
                }else{
                    Toast.makeText(getActivity(), "Username or Password Invalid",
                        Toast.LENGTH_LONG).show();
                }
            }
            val context = this.getContext()
            val inputMethodManager =
                context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
        return binding.root
    }


}
