package com.example.car.ui.main

data class Car(var number: String = "", var brand: String = "", var name: String = "")